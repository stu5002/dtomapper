package com.stu5002.dtoMapper;

import com.stu5002.dtoMapper.annotations.MappedClass;
import com.stu5002.dtoMapper.annotations.MappedField;
import com.stu5002.dtoMapper.annotations.PostProcessing;
import com.stu5002.dtoMapper.exceptions.DtoMapperException;
import com.stu5002.dtoMapper.processors.PostProcessor;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;

import java.lang.reflect.*;
import java.util.*;

import static com.stu5002.dtoMapper.Utils.containsIdentical;
import static com.stu5002.dtoMapper.Utils.firstPartOfPath;
import static com.stu5002.dtoMapper.Utils.isNullOrEmpty;

/**
 * Created by stu5002 on 01.12.15.
 */
public class ReflectionHelper {

    public static Class getMappedClass(Class sourceClass){
        MappedClass mappedClassAnnotation = (MappedClass) sourceClass.getAnnotation(MappedClass.class);
        if(mappedClassAnnotation == null){
            throw new DtoMapperException(sourceClass.getName()+" not annotated with \""+MappedClass.class.getSimpleName()+"\"");
        }
        if(mappedClassAnnotation.value() != MappedClass.UnspecifiedClass.class){
            return mappedClassAnnotation.value();
        }else {
            return null;
        }
    }

    public static List<Field> getAllMappedFields(Class clazz){
        List<Field> allFields = new ArrayList<Field>();
        while(clazz != null){
            for(Field field : clazz.getDeclaredFields()){
                if(field.getAnnotation(MappedField.class) != null){
                    allFields.add(field);
                }
            }
            clazz = clazz.getSuperclass();
        }
        return allFields;
    }

    public static boolean isMappedField(Field field) {
        Class fieldType = field.getType();
        if(Collection.class.isAssignableFrom(field.getType())){
            fieldType = ReflectionHelper.getUnitClass(field);
        }else if(field.getType().isArray()){
            fieldType = getUnitClass(field);
        }
        return isMappedClass(fieldType);
    }

    public static String getPathFromMappedField(MappedField mappedFieldAnnotation, Field annotatedField){
        String fieldPath;
        if(!mappedFieldAnnotation.value().isEmpty()){
            fieldPath = mappedFieldAnnotation.value();
        }else{
            fieldPath = annotatedField.getName();
        }
        return fieldPath;
    }

    public static Class getUnitClass(Field field) {
        Class fieldType = field.getType();
        if(Collection.class.isAssignableFrom(fieldType)){
            ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
            return (Class) stringListType.getActualTypeArguments()[0];
        }else if(fieldType.isArray()){
            return fieldType.getComponentType();
        }else{
            return fieldType;
        }
    }

    public static boolean isMappedClass(Class clazz) {
        MappedClass mappedClassAnnotation = (MappedClass) clazz.getAnnotation(MappedClass.class);
        return mappedClassAnnotation != null;
    }

    public static Object getFieldValueByPath(Object source, String path, FieldsAccessProperties fieldsAccessProperties){
        String fieldName = firstPartOfPath(path);

        Object sourceFieldValue = null;
        switch(fieldsAccessProperties.getRetrievingMod()){
            case METHODS:
                sourceFieldValue = getValueByGetter(source, fieldName);
                break;
            case VALUES:
                sourceFieldValue = getValueDirectly(source, fieldName);
                break;
        }

        if(sourceFieldValue == null || !path.contains(".")){
            return sourceFieldValue;
        }else{
            path = path.substring(path.indexOf('.') + 1);
            return getFieldValueByPath(sourceFieldValue, path, fieldsAccessProperties);
        }
    }

    public static Collection tryGetFieldCollection(Object source, Field field, FieldsAccessProperties fieldsAccessProperties){
        try {
            Collection fieldCollection = (Collection) getFieldValueByPath(source, field.getName(), fieldsAccessProperties);
            return fieldCollection;
        } catch (Exception e) { }
        return null;
    }

    private static Object getValueByGetter(Object source, String fieldName){
        String methodName = "get"+Utils.firstLetterToUpperCase(fieldName);
        Method sourceGetterMethod = findMethod(source.getClass(), methodName);
        return invokeMethod(source, sourceGetterMethod);
    }

    private static Object getValueDirectly(Object source, String fieldName){
        Field field = findField(source.getClass(), fieldName);
        field.setAccessible(true);
        try {
            return field.get(source);
        } catch (Exception e) {
            return null;
        }
    }


    public static void setFieldValueByPath(Object target, String path, Object value, FieldsAccessProperties fieldsAccessProperties) {
        String fieldName = firstPartOfPath(path);
        boolean isPathEnd = !path.contains(".");
        if(isPathEnd){
            switch(fieldsAccessProperties.getInsertionMod()){
                case METHODS:
                    if(isPathEnd){
                        setValueBySetter(target, fieldName, value);
                    }else{
                        Object newPathPart = setNewValueBySetter(target, fieldName);
                        setFieldValueByPath(newPathPart, path.substring(path.indexOf('.')+1), value, fieldsAccessProperties);
                    }
                    break;
                case VALUES:
                    if(isPathEnd){
                        setValueDirectly(target, fieldName, value);
                    }else{
                        Object newPathPart = setNewValueDirectly(target, fieldName);
                        setFieldValueByPath(newPathPart, path.substring(path.indexOf('.') + 1), value, fieldsAccessProperties);
                    }
                    break;
            }
        }else{
            Object existPathPart = getFieldValueByPath(target, fieldName, fieldsAccessProperties);
            setFieldValueByPath(existPathPart, path.substring(path.indexOf('.')+1), value, fieldsAccessProperties);
        }
    }

    private static void setValueBySetter(Object target, String fieldName, Object value) {
        String methodName = "set"+Utils.firstLetterToUpperCase(fieldName);
        Method targetSetterMethod = findMethod(target.getClass(), methodName);
        invokeMethod(target, targetSetterMethod, value);
    }

    private static Object setNewValueBySetter(Object target, String fieldName) {
        String methodName = "set"+Utils.firstLetterToUpperCase(fieldName);
        Method targetSetterMethod = findMethod(target.getClass(), methodName);
        Class setterArgumentType = targetSetterMethod.getParameterTypes()[0];
        Object newValue = createNewInstance(setterArgumentType);
        invokeMethod(target, targetSetterMethod);
        return newValue;
    }

    private static void setValueDirectly(Object source, String fieldName, Object value) {
        if(source == null) return;

        Field field = findField(source.getClass(), fieldName);
        field.setAccessible(true);
        try {
            field.set(source, value);
        } catch (Exception e) {
        }
    }

    private static Object setNewValueDirectly(Object source, String fieldName) {
        Field field = findField(source.getClass(), fieldName);
        field.setAccessible(true);
        Object newValue = createNewInstance(field.getDeclaringClass());
        setValueDirectly(source, fieldName, newValue);
        return newValue;
    }

    public static Object createNewInstance(Class clazz){
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            return null;
        }
    }

    private static Object invokeMethod(Object object, Method method){
        if(method == null) return null;

        try {
            return method.invoke(object);
        } catch (Exception e) {
            return null;
        }
    }

    private static Object invokeMethod(Object object, Method method, Object... args){
        try {
            return method.invoke(object, args);
        } catch (Exception e) {
            return null;
        }
    }


    public static Field findField(Object source, String path) {
        return findField(source.getClass(), path);
    }

    public static Field findField(Class clazz, String name){
        name = firstPartOfPath(name);
        while(clazz != null){
            for(Field field : clazz.getDeclaredFields()){
                if(field.getName().equals(name)){
                    return field;
                }
            }
            clazz = clazz.getSuperclass();
        }
        return null;
    }

    public static Method findMethod(Class clazz, String name){
        while(clazz != null){
            for(Method method : clazz.getDeclaredMethods()){
                if(method.getName().equals(name)){
                    return method;
                }
            }
            clazz = clazz.getSuperclass();
        }
        return null;
    }


    public static PostProcessor getPostProcessor(Object dtoObject){
        if(dtoObject == null) return null;

        Class dtoClass = dtoObject.getClass();
        PostProcessing postProcessingAnno = (PostProcessing) dtoClass.getAnnotation(PostProcessing.class);

        if(postProcessingAnno != null){
            Class postProcessorClass = postProcessingAnno.value();
            if(postProcessorClass.equals(dtoClass)){
                return (PostProcessor) dtoObject;
            }else{
                return (PostProcessor) createNewInstance(postProcessorClass);
            }
        }else if(PostProcessor.class.isAssignableFrom(dtoClass)){
            return (PostProcessor) dtoObject;
        }else{
            return null;
        }
    }

    public static boolean isTagsMatches(MappedField mappedField, String[] tags){
        String[] excludeTags = mappedField.excludeTags();
        String[] includeTags = mappedField.includeTags();
        return (!containsIdentical(excludeTags, tags)) && (isNullOrEmpty(includeTags) || containsIdentical(includeTags, tags));
    }

}
