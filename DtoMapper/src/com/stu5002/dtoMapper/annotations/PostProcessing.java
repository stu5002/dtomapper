package com.stu5002.dtoMapper.annotations;

import com.stu5002.dtoMapper.processors.PostProcessor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by stu5002 on 08.02.16.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface PostProcessing {
    Class<? extends PostProcessor> value();
}
