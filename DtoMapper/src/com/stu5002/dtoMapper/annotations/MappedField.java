package com.stu5002.dtoMapper.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by stu5002 on 28.11.15.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface MappedField {

    String value() default "";

    boolean withoutMapping() default false;

    boolean toDtoIgnore() default false;

    boolean toDomainIgnore() default false;

    String[] includeTags() default {};

    String[] excludeTags() default {};
}
