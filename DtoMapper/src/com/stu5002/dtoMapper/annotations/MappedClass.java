package com.stu5002.dtoMapper.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by stu5002 on 28.11.15.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface MappedClass {
    Class value() default UnspecifiedClass.class;

    class UnspecifiedClass{}
}

