package com.stu5002.dtoMapper.substitution;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by stu5002 on 02.01.16.
 */
public interface SubstitutionPerformer {
    Object findSubstitution(Class clazz, Map<Field, Object> identifiers, Object userData);
}
