package com.stu5002.dtoMapper;

import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;
import com.stu5002.dtoMapper.substitution.SubstitutionPerformer;

import java.util.Collection;

/**
 * Created by stu5002 on 09.03.16.
 */
public interface Mapper {
    Mapper withSubstitutionPerformer(SubstitutionPerformer substitutionPerformer);

    Mapper withFieldsAccessProperties(FieldsAccessProperties fieldsAccessProperties);

    Mapper withTags(String... tags);

    Mapper withUserData(Object userData);

    Object getUserData();

    <T> T toDto(Object source);

    <T> T[] toDto(Object[] source);

    <T extends Collection<E>, E> T toDto(Collection source);

    <T> T toDto(Object source, Class<T> targetClass);

    <T> T[] toDto(Object[] source, Class<T> targetClass);

    <T extends Collection<E>, E> T toDto(Collection source, Class<E> targetClass);

    <T> T toDto(Object source, T target);

    <T> T toDomain(Object source);

    <T> T[] toDomain(Object[] source);

    <T extends Collection<E>, E> T toDomain(Collection source);

    <T> T toDomain(Object source, Class<T> targetClass);

    <T> T[] toDomain(Object[] source, Class<T> targetClass);

    <T extends Collection<E>, E> T toDomain(Collection source, Class<E> targetClass);

    <T> T toDomain(Object source, T target);
}
