package com.stu5002.dtoMapper.processors;
import com.stu5002.dtoMapper.Mapper;

/**
 * Created by stu5002 on 08.02.16.
 */
public interface PostProcessor {

    void afterToDtoMapping(Object domainObject, Object dtoObject, Mapper mapper);

    void afterToDomainMapping(Object domainObject, Object dtoObject, Mapper mapper);
}
