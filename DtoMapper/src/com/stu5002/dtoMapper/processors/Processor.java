package com.stu5002.dtoMapper.processors;

import com.stu5002.dtoMapper.Mapper;
import com.stu5002.dtoMapper.ReflectionHelper;
import com.stu5002.dtoMapper.exceptions.DtoMapperException;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 * Created by stu5002 on 02.12.15.
 */
public abstract class Processor {

    private FieldsAccessProperties properties;

    private String[] tags;

    private Object userData;

    protected ProcessedCache processedObjects = new ProcessedCache();

    public Processor(FieldsAccessProperties properties) {
        this.properties = properties;
    }

    public <T> T[] process(Object[] source) {
        if(source == null) return null;

        Class targetUnitClass = ReflectionHelper.getMappedClass(source.getClass().getComponentType());
        if(processedObjects.contains(source, targetUnitClass)){
            return (T[]) processedObjects.get(source, targetUnitClass);
        }

        return (T[]) process(source, targetUnitClass);
    }

    public <T> T[] process(Object[] source, Class<T> targetUnitClass) {
        if(source == null) return null;

        if(processedObjects.contains(source, targetUnitClass)){
            return (T[]) processedObjects.get(source, targetUnitClass);
        }

        try {
            T[] target = (T[]) Array.newInstance(targetUnitClass, source.length);
            processedObjects.put(source, target, targetUnitClass);
            for(int i = 0; i < source.length; i++){
                target[i] = process(source[i], targetUnitClass);
            }
            return target;
        } catch (Exception e) {
            DtoMapperException dtoMapperException = new DtoMapperException("Error while creating new instance of "+source.getClass().getName());
            dtoMapperException.initCause(e);
            throw dtoMapperException;
        }
    }

    public <T extends Collection<E>, E> T process(Collection source, Class<E> targetUnitClass) {
        return process(source, targetUnitClass, null);
    }

    public <T extends Collection<E>, E> T process(Collection source, Class<E> targetUnitClass, T target) {
        if(source == null) return null;

        if(processedObjects.contains(source, targetUnitClass)){
            return (T) processedObjects.get(source, targetUnitClass);
        }

        try {
            if(target == null){
                target = (T) source.getClass().newInstance();
            }
            processedObjects.put(source, target);
            for(Object sourceUnit : source){
                target.add(process(sourceUnit, targetUnitClass));
            }
            return target;
        } catch (Exception e) {
            DtoMapperException dtoMapperException = new DtoMapperException("Error while creating new instance of "+source.getClass().getName());
            dtoMapperException.initCause(e);
            throw dtoMapperException;
        }
    }

    public void postprocessObjects(Mapper mapper) {
        for(Map.Entry<Object, Map<Class, Object>> cachePart : processedObjects.getCache().entrySet()){
            Object source = cachePart.getKey();
            for(Map.Entry<Class, Object> processedObject: cachePart.getValue().entrySet()){
                postprocessObject(source, processedObject.getValue(), mapper);
            }
        }
    }

    public <T extends Collection> T process(Collection source){
        if(source == null) return null;

        Class targetClass = null;
        if(source.isEmpty()){
            targetClass = source.getClass();
        }else{
            targetClass = retrieveMappedClassValue(source.iterator().next().getClass());
        }

        if(processedObjects.contains(source, targetClass)){
            return (T) processedObjects.get(source, targetClass);
        }

        try {
            T target = (T) source.getClass().newInstance();
            processedObjects.put(source, target);
            for (Object sourceUnit : source) {
                Object targetUnit = process(sourceUnit);
                target.add(targetUnit);
            }
            return target;
        } catch (Exception e) {
            DtoMapperException dtoMapperException = new DtoMapperException("Error while creating new instance of "+source.getClass().getName());
            dtoMapperException.initCause(e);
            throw dtoMapperException;
        }
    }

    protected Class retrieveMappedClassValue(Class<?> sourceClass){
        Class targetClass = ReflectionHelper.getMappedClass(sourceClass);

        if(targetClass == null){
            throw new DtoMapperException("Not specified mapped class for "+sourceClass.getName());
        }else{
            return targetClass;
        }
    }

    public abstract <T> T process(Object source, Class<T> targetClass);

    public abstract <T> T process(Object source);

    public abstract <T> T process(Object source, T target);

    protected abstract void postprocessObject(Object source, Object target, Mapper mapper);

    public FieldsAccessProperties getProperties() {
        return properties;
    }

    public void setProperties(FieldsAccessProperties properties) {
        this.properties = properties;
    }

    public ProcessedCache getProcessedObjects() {
        return processedObjects;
    }

    public void setProcessedObjects(ProcessedCache processedObjects) {
        this.processedObjects = processedObjects;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public Object getUserData() {
        return userData;
    }

    public void setUserData(Object userData) {
        this.userData = userData;
    }
}
