package com.stu5002.dtoMapper.processors;

import com.stu5002.dtoMapper.Mapper;
import com.stu5002.dtoMapper.ReflectionHelper;
import com.stu5002.dtoMapper.annotations.MappedField;
import com.stu5002.dtoMapper.substitution.SubstitutionPerformer;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;
import com.stu5002.dtoMapper.prototyping.DomainPrototypeNode;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

import static com.stu5002.dtoMapper.ReflectionHelper.*;

/**
 * Created by stu5002 on 06.12.15.
 */
public class ToDomainProcessor extends Processor {

    private SubstitutionPerformer substitutionPerformer;

    private ProcessedCache prototypedObjects = new ProcessedCache();

    public ToDomainProcessor(FieldsAccessProperties properties) {
        super(properties);
    }

    @Override
    public <T> T process(Object source) {
        if(source == null){
            return null;
        }

        Class<T> targetClass = retrieveMappedClassValue(source.getClass());
        return process(source, targetClass);
    }

    @Override
    public <T> T process(Object source, Class<T> targetClass) {
        if(source == null) return null;

        DomainPrototypeNode node = createPrototype(source, targetClass);
        T target = createByPrototype(node, targetClass);
        return target;
    }

    @Override
    public  <T> T process(Object source, T target){
        if(source == null) return target;
        if(target == null) return null;

        DomainPrototypeNode node = createPrototype(source, target.getClass(), true);
        return updateByPrototype(node, target);
    }

    @Override
    public void postprocessObject(Object source, Object target, Mapper mapper) {
        if(source.getClass() == DomainPrototypeNode.class) {
            DomainPrototypeNode prototypeNode = (DomainPrototypeNode) source;
            if(prototypeNode.getSource() != null){
                PostProcessor postProcessor = ReflectionHelper.getPostProcessor(prototypeNode.getSource());
                if(postProcessor != null){
                    postProcessor.afterToDomainMapping(target, prototypeNode.getSource(), mapper);
                }
            }
        }
    }


    public DomainPrototypeNode createPrototype(Object source, Class targetClass){
        return createPrototype(source, targetClass, false);
    }

    public DomainPrototypeNode createPrototype(Object source, Class targetClass, boolean ignoreRootMerge){
        if(prototypedObjects.contains(source, DomainPrototypeNode.class)){
            return (DomainPrototypeNode) prototypedObjects.get(source, DomainPrototypeNode.class);
        }
        DomainPrototypeNode result = new DomainPrototypeNode(source);
        prototypedObjects.put(source, result);

        List<Field> allSourceFields = ReflectionHelper.getAllMappedFields(source.getClass());
        for(Field sourceField : allSourceFields){
            MappedField mappedFieldAnno = sourceField.getAnnotation(MappedField.class);
            if(!mappedFieldAnno.toDomainIgnore() && isTagsMatches(mappedFieldAnno, getTags())){
                String mappedFieldPath = ReflectionHelper.getPathFromMappedField(mappedFieldAnno, sourceField);
                Field targetField = ReflectionHelper.findField(targetClass, mappedFieldPath);
                if(targetField != null){
                    projectValueToPrototype(result, source, sourceField, targetField, mappedFieldPath, mappedFieldAnno);
                }
            }
        }
        return result;
    }

    private void projectValueToPrototype(DomainPrototypeNode result, Object source, Field sourceField, Field targetField, String mappedFieldPath, MappedField mappedFieldAnno) {
        Object mappedFieldValue = ReflectionHelper.getFieldValueByPath(source, sourceField.getName(), getProperties());

        if(mappedFieldValue != null){
            if(!mappedFieldAnno.withoutMapping() && ReflectionHelper.isMappedField(sourceField) && !(mappedFieldValue instanceof Collection) && !mappedFieldValue.getClass().isArray()){
                DomainPrototypeNode mappedPrototypeNode = result.getChild(mappedFieldPath);
                Class targetUnitClass = targetField.getType();
                if(mappedPrototypeNode != null){
                    DomainPrototypeNode mergeFrom = createPrototype(mappedFieldValue, targetUnitClass);
                    mergeFrom.setName(mappedFieldPath);
                    mergePrototypes(mergeFrom, mappedPrototypeNode);
                }else {
                    mappedPrototypeNode = createPrototype(mappedFieldValue, targetUnitClass);
                    mappedPrototypeNode.setName(mappedFieldPath);
                    result.upsertChild(mappedPrototypeNode);
                }
            }else{
                result.setValue(mappedFieldPath, source, mappedFieldValue);
            }
        }
    }

    private void mergePrototypes(DomainPrototypeNode from, DomainPrototypeNode to){
        if(from != null && from.getFieldsValues() != null && to != null){
            for(DomainPrototypeNode fromChild : from.getFieldsValues()){
                to.upsertChild(fromChild);
            }
        }
    }

    public <T> T createByPrototype(DomainPrototypeNode prototype, Class<T> objectClass){
        if(prototypedObjects.contains(prototype, objectClass)){
            return (T) prototypedObjects.get(prototype, objectClass);
        }

        if(objectClass != null){
            T object = findOrCreateInstanceByPrototype(prototype, objectClass);
            return updateByPrototype(prototype, object);
        }else{
            return null;
        }
    }

    public  <T> T updateByPrototype(DomainPrototypeNode prototype, T object){
        if(prototypedObjects.contains(prototype, object.getClass())){
            return (T) prototypedObjects.get(prototype, object.getClass());
        }
        prototypedObjects.put(prototype, object);

        T existObject =  findInstanceByPrototype(prototype, (Class<T>) object.getClass());
        if(existObject != null){
            object = existObject;
        }

        if(prototype != null && prototype.getFieldsValues() != null && object != null){
            for(DomainPrototypeNode childPrototype : prototype.getFieldsValues()){
                projectPrototypeToValue(childPrototype, object);
            }
        }
        getProcessedObjects().put(prototype, object);
        return object;
    }

    private void projectPrototypeToValue(DomainPrototypeNode prototype, Object object){
        Field mappedField = ReflectionHelper.findField(object, prototype.getName());

        Object fieldValue = ReflectionHelper.getFieldValueByPath(object, prototype.getName(), getProperties());

        if(prototype.isLeaf()){
            Field targetField = ReflectionHelper.findField(object, prototype.getName());
            if(targetField == null) return;

            fieldValue = prototype.getValue();
            Class targetUnitClass = getUnitClass(targetField);
            if(fieldValue instanceof Collection){
                Collection fieldCollection = tryGetFieldCollection(object, targetField, getProperties());
                fieldValue = process((Collection) fieldValue, targetUnitClass, fieldCollection);
            }else if(fieldValue != null && fieldValue.getClass().isArray()){
                fieldValue = process((Object[]) fieldValue, targetUnitClass);
            }
        }else{
            if(fieldValue == null){
                fieldValue = createByPrototype(prototype, mappedField.getType());
            }else{
                fieldValue = updateByPrototype(prototype, fieldValue);
            }
        }

        ReflectionHelper.setFieldValueByPath(object, prototype.getName(), fieldValue, getProperties());
    }

    private <T> T findOrCreateInstanceByPrototype(DomainPrototypeNode prototype, Class<T> objectClass){
        T result =  findInstanceByPrototype(prototype, objectClass);
        if (result != null) {
            return result;
        }

        return (T) createNewInstance(objectClass);
    }

    private <T> T findInstanceByPrototype(DomainPrototypeNode prototype, Class<T> objectClass){
        if(substitutionPerformer != null) {
            T result =  (T) substitutionPerformer.findSubstitution(objectClass, prototype.getIdentifiers(objectClass), this.getUserData());
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    public void setSubstitutionPerformer(SubstitutionPerformer substitutionPerformer) {
        this.substitutionPerformer = substitutionPerformer;
    }
}
