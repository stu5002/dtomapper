package com.stu5002.dtoMapper.processors;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stu5002 on 30.01.16.
 */
public class ProcessedCache {

    private Map<Object, Map<Class, Object>> cache = new HashMap<Object, Map<Class, Object>>();

    public void clear(){
        cache.clear();
    }

    public boolean contains(Object source, Class targetClass){
        return cache.containsKey(source) && cache.get(source).containsKey(targetClass);
    }

    public Object get(Object source, Class targetClass){
        if(contains(source, targetClass)){
            return cache.get(source).get(targetClass);
        }
        return null;
    }

    public void put(Object source, Object target){
        put(source, target, target.getClass());
    }

    public void put(Object source, Object target, Class targetClass){
        if(source != null && target != null){
            if(!cache.containsKey(source)){
                cache.put(source, new HashMap<Class, Object>());
            }
            if(!cache.get(source).containsKey(targetClass)){
                cache.get(source).put(targetClass, target);
            }
        }
    }

    public Map<Object, Map<Class, Object>> getCache() {
        return cache;
    }

    public void setCache(Map<Object, Map<Class, Object>> cache) {
        this.cache = cache;
    }
}
