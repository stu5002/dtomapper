package com.stu5002.dtoMapper.processors;

import com.stu5002.dtoMapper.Mapper;
import com.stu5002.dtoMapper.ReflectionHelper;
import com.stu5002.dtoMapper.annotations.MappedField;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;

import static com.stu5002.dtoMapper.ReflectionHelper.*;

/**
 * Created by stu5002 on 02.12.15.
 */
public class ToDtoProcessor extends Processor{

    public ToDtoProcessor(FieldsAccessProperties properties) {
        super(properties);
    }

    @Override
    public <T> T process(Object source){
        if(source == null) return null;

        Class<T> targetClass = retrieveMappedClassValue(source.getClass());
        if(processedObjects.contains(source, targetClass)){
            return (T) processedObjects.get(source, targetClass);
        }
        T target = process(source, (T) createNewInstance(targetClass));
        processedObjects.put(source, target);
        return target;
    }

    @Override
    public <T> T process(Object source, T target){
        if(source == null) return target;
        if (target == null) return null;

        Class targetClass = target.getClass();
        if(processedObjects.contains(source, targetClass)){
            return (T) processedObjects.get(source, targetClass);
        }else{
            processedObjects.put(source, target);
        }

        List<Field> allTargetFields = ReflectionHelper.getAllMappedFields(targetClass);

        for(Field targetField : allTargetFields){
            setField(source, target, targetField);
        }

        return target;
    }

    @Override
    public void postprocessObject(Object source, Object target, Mapper mapper) {
        PostProcessor postProcessor = ReflectionHelper.getPostProcessor(target);
        if(postProcessor != null){
            postProcessor.afterToDtoMapping(source, target, mapper);
        }
    }

    public <T> T process(Object source, Class<T> targetClass) {
        if(source == null) return null;

        T target = (T) createNewInstance(targetClass);
        return process(source, target);
    }

    private void setField(Object source, Object target, Field targetField){
        MappedField mappedFieldAnno = targetField.getAnnotation(MappedField.class);
        if(!mappedFieldAnno.toDtoIgnore() && isTagsMatches(mappedFieldAnno, getTags())){
            String mappedFieldPath = ReflectionHelper.getPathFromMappedField(mappedFieldAnno, targetField);

            Object mappedFieldValue = ReflectionHelper.getFieldValueByPath(source, mappedFieldPath, getProperties());
            //TODO Здесь проверять класс конвертируемого поля всегда только ДТО объекта, так как domain-объект может быть недоступен для пометки как MappedClass (например из чужой библиотеки)

            if(mappedFieldValue != null && ReflectionHelper.isMappedField(targetField) && !mappedFieldAnno.withoutMapping()){
                if(mappedFieldValue instanceof Collection){
                    Class unitClass = getUnitClass(targetField);
                    Collection fieldCollection = tryGetFieldCollection(target, targetField, getProperties());
                    mappedFieldValue = process((Collection) mappedFieldValue, unitClass, fieldCollection);
                }else if(mappedFieldValue.getClass().isArray()){
                    Class unitClass = getUnitClass(targetField);
                    mappedFieldValue = process((Object[]) mappedFieldValue, unitClass);
                }else {
                    mappedFieldValue = process(mappedFieldValue, targetField.getType());
                }
            }
            ReflectionHelper.setFieldValueByPath(target, targetField.getName(), mappedFieldValue, getProperties());
        }

    }
}
