package com.stu5002.dtoMapper.processors.properties;

/**
 * Created by stu5002 on 26.01.16.
 */
public class FieldsAccessProperties {

    private FiledsAccessMod retrievingMod = FiledsAccessMod.METHODS;

    private FiledsAccessMod insertionMod = FiledsAccessMod.VALUES;

    public FiledsAccessMod getRetrievingMod() {
        return retrievingMod;
    }

    public void setRetrievingMod(FiledsAccessMod retrievingMod) {
        if(retrievingMod != null){
            this.retrievingMod = retrievingMod;
        }
    }

    public FiledsAccessMod getInsertionMod() {
        return insertionMod;
    }

    public void setInsertionMod(FiledsAccessMod insertionMod) {
        if(insertionMod != null){
            this.insertionMod = insertionMod;
        }
    }
}
