package com.stu5002.dtoMapper.processors.properties;

/**
 * Created by stu5002 on 26.01.16.
 */
public enum FiledsAccessMod {
    METHODS,
    VALUES
}
