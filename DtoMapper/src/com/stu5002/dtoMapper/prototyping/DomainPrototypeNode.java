package com.stu5002.dtoMapper.prototyping;

import com.stu5002.dtoMapper.ReflectionHelper;
import com.stu5002.dtoMapper.Utils;

import java.lang.reflect.Field;
import java.util.*;

import static com.stu5002.dtoMapper.Utils.firstPartOfPath;

/**
 * Created by stu5002 on 07.01.16.
 */
public class DomainPrototypeNode {

    private Object source;

    private String name;

    private Object value;

    private List<DomainPrototypeNode> fieldsValues;

    public DomainPrototypeNode(Object source) {
        this.source = source;
    }

    public boolean isLeaf(){
        return fieldsValues == null;
    }

    public void setValue(String path, Object source, Object value) {
        if(fieldsValues == null){
            fieldsValues = new ArrayList<DomainPrototypeNode>();
        }
        String firstPathPart = firstPartOfPath(path);
        DomainPrototypeNode childNode = getChild(firstPathPart);
        if(childNode == null){
            childNode = new DomainPrototypeNode(source);
        }

        if(path.contains(".")){
            childNode.setName(firstPathPart);
            childNode.setValue(path.substring(path.indexOf('.')+1), null, value);
        }else{
            childNode.setName(path);
            childNode.setValue(value);
        }
        upsertChild(childNode);
    }

    public void upsertChild(DomainPrototypeNode child) {
        if(child == null) return;
        if(fieldsValues == null){
            fieldsValues = new ArrayList<DomainPrototypeNode>();
        }
        for(DomainPrototypeNode existsChild : fieldsValues){
            if(Utils.isNpEquals(existsChild.getName(), child.getName())){
                existsChild.setValue(child.getValue());
                return;
            }
        }
        fieldsValues.add(child);
    }

    public DomainPrototypeNode getChild(String childName) {
        if(fieldsValues == null) return null;
        DomainPrototypeNode result = null;
        for(DomainPrototypeNode child : fieldsValues){
            if(Utils.isNpEquals(childName, child.getName())){
                result = child;
                break;
            }
        }
        return result;
    }

    public Map<Field, Object> getIdentifiers(Class objectClass){
        if(fieldsValues == null && objectClass == null) return null;
        Map<Field, Object> identifiers = new HashMap<Field, Object>();
        for(DomainPrototypeNode child: fieldsValues){
            if(child.isLeaf() && child.getName() != null && child.getValue() != null && !(child.getValue() instanceof Collection)){
                Field field = ReflectionHelper.findField(objectClass, child.getName());
                if(field != null){
                    identifiers.put(field, child.getValue());
                }
            }
        }
        return identifiers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public List<DomainPrototypeNode> getFieldsValues() {
        return fieldsValues;
    }

    public void setFieldsValues(List<DomainPrototypeNode> fieldsValues) {
        this.fieldsValues = fieldsValues;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }
}
