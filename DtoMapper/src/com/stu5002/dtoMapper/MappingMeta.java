package com.stu5002.dtoMapper;

import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;
import com.stu5002.dtoMapper.substitution.SubstitutionPerformer;

import java.util.Collection;

/**
 * Created by stu5002 on 09.03.16.
 */
public class MappingMeta implements Mapper {

    private SubstitutionPerformer substitutionPerformer = null;

    private FieldsAccessProperties fieldsAccessProperties = null;

    private String[] tags;

    private Object userData;

    @Override
    public Mapper withSubstitutionPerformer(SubstitutionPerformer substitutionPerformer) {
        this.substitutionPerformer = substitutionPerformer;
        return this;
    }

    @Override
    public Mapper withFieldsAccessProperties(FieldsAccessProperties fieldsAccessProperties) {
        this.fieldsAccessProperties = fieldsAccessProperties;
        return this;
    }

    @Override
    public Mapper withTags(String... tags) {
        this.tags = tags;
        return this;
    }

    @Override
    public Mapper withUserData(Object userData) {
        this.userData = userData;
        return this;
    }

    @Override
    public Object getUserData() {
        return userData;
    }

    private void injectMetaToProcessor(Processor processor){
        if(processor.getProperties() == null){
            processor.setProperties(new FieldsAccessProperties());
        }
        processor.setTags(tags);
        processor.setUserData(userData);
        if(processor instanceof ToDomainProcessor){
            ((ToDomainProcessor) processor).setSubstitutionPerformer(substitutionPerformer);
        }
    }

    @Override
    public <T> T toDto(Object source){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T[] toDto(Object[] source){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T[] result = processor.process(source);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T extends Collection<E>, E> T toDto(Collection source){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T toDto(Object source, Class<T> targetClass){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source, targetClass);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T[] toDto(Object[] source, Class<T> targetClass){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T[] result = processor.process(source, targetClass);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T extends Collection<E>, E> T toDto(Collection source, Class<E> targetClass){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source, targetClass);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T toDto(Object source, T target){
        ToDtoProcessor processor = new ToDtoProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source, target);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T toDomain(Object source){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T[] toDomain(Object[] source){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T[] result = processor.process(source);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T extends Collection<E>, E> T toDomain(Collection source){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T toDomain(Object source, Class<T> targetClass){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source, targetClass);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T[] toDomain(Object[] source, Class<T> targetClass){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T[] result = processor.process(source, targetClass);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T extends Collection<E>, E> T toDomain(Collection source, Class<E> targetClass){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source, targetClass);
        processor.postprocessObjects(this);
        return result;
    }

    @Override
    public <T> T toDomain(Object source, T target){
        ToDomainProcessor processor = new ToDomainProcessor(fieldsAccessProperties);
        injectMetaToProcessor(processor);
        T result = processor.process(source, target);
        processor.postprocessObjects(this);
        return result;
    }
}
