package com.stu5002.dtoMapper;


import com.stu5002.dtoMapper.substitution.SubstitutionPerformer;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;

import java.util.Collection;

/**
 * Created by stu5002 on 28.11.15.
 */
public class DtoMapper implements Mapper {

    private SubstitutionPerformer substitutionPerformer = null;

    private FieldsAccessProperties toDtoFieldsAccessProperties = new FieldsAccessProperties();

    private FieldsAccessProperties toDomainFieldsAccessProperties = new FieldsAccessProperties();


    private MappingMeta generateToDomainDefaultMeta(){
        MappingMeta meta = new MappingMeta();
        meta.withFieldsAccessProperties(toDomainFieldsAccessProperties)
                .withSubstitutionPerformer(substitutionPerformer);
        return meta;
    }

    private MappingMeta generateToDtoDefaultMeta(){
        MappingMeta meta = new MappingMeta();
        meta.withFieldsAccessProperties(toDtoFieldsAccessProperties)
                .withSubstitutionPerformer(substitutionPerformer);
        return meta;
    }

    public SubstitutionPerformer getSubstitutionPerformer() {
        return substitutionPerformer;
    }

    public void setSubstitutionPerformer(SubstitutionPerformer substitutionPerformer) {
        this.substitutionPerformer = substitutionPerformer;
    }

    public FieldsAccessProperties getToDtoFieldsAccessProperties() {
        return toDtoFieldsAccessProperties;
    }

    public void setToDtoFieldsAccessProperties(FieldsAccessProperties toDtoFieldsAccessProperties) {
        this.toDtoFieldsAccessProperties = toDtoFieldsAccessProperties;
    }

    public FieldsAccessProperties getToDomainFieldsAccessProperties() {
        return toDomainFieldsAccessProperties;
    }

    public void setToDomainFieldsAccessProperties(FieldsAccessProperties toDomainFieldsAccessProperties) {
        this.toDomainFieldsAccessProperties = toDomainFieldsAccessProperties;
    }

    @Override
    public Mapper withSubstitutionPerformer(SubstitutionPerformer substitutionPerformer) {
        return new MappingMeta().withSubstitutionPerformer(substitutionPerformer);
    }

    @Override
    public Mapper withFieldsAccessProperties(FieldsAccessProperties fieldsAccessProperties) {
        return new MappingMeta().withFieldsAccessProperties(fieldsAccessProperties);
    }

    @Override
    public Mapper withTags(String... tags) {
        return new MappingMeta().withTags(tags);
    }

    @Override
    public Mapper withUserData(Object userData) {
        return new MappingMeta().withUserData(userData);
    }

    @Override
    public Object getUserData() {
        return null;
    }

    @Override
    public <T> T toDto(Object source) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source);
    }

    @Override
    public <T> T[] toDto(Object[] source) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source);
    }

    @Override
    public <T extends Collection<E>, E> T toDto(Collection source) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source);
    }

    @Override
    public <T> T toDto(Object source, Class<T> targetClass) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source, targetClass);
    }

    @Override
    public <T> T[] toDto(Object[] source, Class<T> targetClass) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source, targetClass);
    }

    @Override
    public <T extends Collection<E>, E> T toDto(Collection source, Class<E> targetClass) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source, targetClass);
    }

    @Override
    public <T> T toDto(Object source, T target) {
        MappingMeta meta = generateToDtoDefaultMeta();
        return meta.toDto(source, target);
    }

    @Override
    public <T> T toDomain(Object source) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source);
    }

    @Override
    public <T> T[] toDomain(Object[] source) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source);
    }

    @Override
    public <T extends Collection<E>, E> T toDomain(Collection source) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source);
    }

    @Override
    public <T> T toDomain(Object source, Class<T> targetClass) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source, targetClass);
    }

    @Override
    public <T> T[] toDomain(Object[] source, Class<T> targetClass) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source, targetClass);
    }

    @Override
    public <T extends Collection<E>, E> T toDomain(Collection source, Class<E> targetClass) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source, targetClass);
    }

    @Override
    public <T> T toDomain(Object source, T target) {
        MappingMeta meta = generateToDomainDefaultMeta();
        return meta.toDomain(source, target);
    }
}
