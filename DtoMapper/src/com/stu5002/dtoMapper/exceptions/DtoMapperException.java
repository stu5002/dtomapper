package com.stu5002.dtoMapper.exceptions;

/**
 * Created by stu5002 on 28.11.15.
 */
public class DtoMapperException extends RuntimeException  {

    public DtoMapperException(String message) {
        super(message);
    }

    public DtoMapperException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
