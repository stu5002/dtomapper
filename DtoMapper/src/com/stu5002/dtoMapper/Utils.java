package com.stu5002.dtoMapper;

/**
 * Created by stu5002 on 01.12.15.
 */
public class Utils {
    public static boolean isNpEquals(Object o1, Object o2){
        if(o1 == null){
            return o2 == null;
        }else{
            return o2 != null && o1.equals(o2);
        }
    }

    public static String firstLetterToUpperCase(String value){
        return value.substring(0, 1).toUpperCase() + value.substring(1);
    }

    public static String firstPartOfPath(String path){
        return path.split("\\.")[0];
    }

    public static boolean containsIdentical(String[] arr1, String[] arr2){
        if(arr1 == null || arr2 == null) return false;

        for(String unit1 : arr1){
            for(String unit2 : arr2){
                if(unit1.equals(unit2)){
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean isNullOrEmpty(String[] arr){
        return arr == null || arr.length == 0;
    }
}
