package entities.domain;

import com.stu5002.dtoMapper.annotations.MappedClass;
import entities.dto.CitizenDto;

/**
 * Created by stu5002 on 15.12.15.
 */
@MappedClass(CitizenDto.class)
public class Citizen{
    private String name;

    private String surname;

    private String phone;

    private int age;

    private Honorific honorific;

    private Street street;

    private String fullName;

    public Citizen(){}

    public Citizen(Street street, String name, String surname, String phone, int age, Honorific honorific) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.age = age;
        this.honorific = honorific;
        if(street != null){
            street.getTenants().add(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Citizen citizen = (Citizen) o;

        if (getAge() != citizen.getAge()) return false;
        if (getName() != null ? !getName().equals(citizen.getName()) : citizen.getName() != null) return false;
        if (getSurname() != null ? !getSurname().equals(citizen.getSurname()) : citizen.getSurname() != null)
            return false;
        if (getPhone() != null ? !getPhone().equals(citizen.getPhone()) : citizen.getPhone() != null) return false;
        if (getHonorific() != citizen.getHonorific()) return false;
        return !(getStreet() != null ? !getStreet().equals(citizen.getStreet()) : citizen.getStreet() != null);

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getSurname() != null ? getSurname().hashCode() : 0);
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        result = 31 * result + getAge();
        result = 31 * result + (getHonorific() != null ? getHonorific().hashCode() : 0);
        result = 31 * result + (getStreet() != null ? getStreet().hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Honorific getHonorific() {
        return honorific;
    }

    public void setHonorific(Honorific honorific) {
        this.honorific = honorific;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
