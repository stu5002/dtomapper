package entities.domain;

import com.stu5002.dtoMapper.annotations.MappedClass;
import entities.dto.CityDto;

/**
 * Created by stu5002 on 15.12.15.
 */
@MappedClass(CityDto.class)
public class City {
    private String name;

    private Citizen mayor;

    private Street[] streets;

    public City() {
    }

    public City(String name, Citizen mayor) {
        this.name = name;
        this.mayor = mayor;
        setStreets(new Street[20]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Citizen getMayor() {
        return mayor;
    }

    public void setMayor(Citizen mayor) {
        this.mayor = mayor;
    }

    public Street[] getStreets() {
        return streets;
    }

    public void setStreets(Street[] streets) {
        this.streets = streets;
    }

    public static City buildJavaville(){
        Citizen mayor = new Citizen(null, "Duke", "Smith", "3483984", 30, Honorific.MR);
        City javaville = new City("Javaville", mayor);

        Street annotationsStreet = new Street(javaville, "Annotations st.");
        Street exceptionsAvenue = new Street(javaville, "Exceptions ave.");

        Citizen mrOverride = new Citizen(annotationsStreet, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen mrsDeprecated = new Citizen(annotationsStreet, "Deprecated", "Williams", "875445", 30, Honorific.MRS);
        Citizen missSuppressWarnings = new Citizen(annotationsStreet, "SuppressWarnings", "Jones", "238710", 30, Honorific.MISS);
        Citizen mrRetention = new Citizen(annotationsStreet, "Retention", "Brown", "082371", 30, Honorific.MR);
        Citizen mrNullPointer = new Citizen(exceptionsAvenue, "Null Pointer", "Devis", "427341", 30, Honorific.MR);
        Citizen mrClassCast = new Citizen(exceptionsAvenue, "Class Cast", "Miller", "348540", 30, Honorific.MR);
        Citizen missUnsupportedOperation = new Citizen(exceptionsAvenue, "Unsupported Operation", "Willson", "438762", 30, Honorific.MISS);
        Citizen mrsNumberFormat = new Citizen(exceptionsAvenue, "Number Format", "Moore", "238766", 30, Honorific.MISS);

        return javaville;
    }
}
