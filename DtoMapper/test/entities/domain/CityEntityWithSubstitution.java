package entities.domain;

import com.stu5002.dtoMapper.annotations.MappedClass;
import entities.dto.CityDto;

/**
 * Created by stu5002 on 28.01.16.
 */
@MappedClass(CityDto.class)
public class CityEntityWithSubstitution extends City{
    public static String spoofedName = "spoofedName";

    @Override
    public String getName(){
        return this.spoofedName;
    }

    @Override
    public void setName(String name){
        super.setName(this.spoofedName);
    }

    public String getRealName(){
        return super.getName();
    }

    public void setRealName(String name){
        super.setName(name);
    }
}
