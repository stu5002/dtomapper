package entities.domain;

/**
 * Created by stu5002 on 15.12.15.
 */
public enum Honorific {
    MR,
    MRS,
    MISS;
}
