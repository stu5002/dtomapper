package entities.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stu5002 on 15.12.15.
 */
public class Street {
    private City city;

    private String name;

    List<Citizen> tenants = new ArrayList<Citizen>();

    private int population = 0;

    public Street() {
    }

    public Street(City city, String name) {
        this.city = city;
        this.name = name;
        setTenants(new ArrayList<Citizen>());
        if(city != null){
            for(int i = 0; i < city.getStreets().length; i++){
                if(city.getStreets()[i] == null){
                    city.getStreets()[i] = this;
                    break;
                }
            }
        }
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Citizen> getTenants() {
        return tenants;
    }

    public void setTenants(List<Citizen> tenants) {
        this.tenants = tenants;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}
