package entities.dto;


/**
 * Created by stu5002 on 18.12.15.
 */
public interface ComparableToDomain {
    <T extends Object> boolean isSuperficiallyEqualsTo(T domainbject);
}
