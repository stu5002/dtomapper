package entities.dto;

import com.stu5002.dtoMapper.annotations.MappedClass;
import com.stu5002.dtoMapper.annotations.MappedField;
import entities.domain.Citizen;
import entities.domain.City;
import entities.domain.Honorific;

/**
 * Created by stu5002 on 15.12.15.
 */
@MappedClass(City.class)
public class CityDto {
    @MappedField
    private String name;

    @MappedField(withoutMapping = true)
    private Citizen mayor;

    @MappedField
    private StreetDto[] streets;

    public CityDto() {
    }

    public CityDto(String name, Citizen mayor) {
        this.name = name;
        this.mayor = mayor;
        setStreets(new StreetDto[20]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Citizen getMayor() {
        return mayor;
    }

    public void setMayor(Citizen mayor) {
        this.mayor = mayor;
    }

    public StreetDto[] getStreets() {
        return streets;
    }

    public void setStreets(StreetDto[] streets) {
        this.streets = streets;
    }

    public static CityDto buildJavaville(){
        Citizen mayor = new Citizen(null, "Duke", "Smith", "3483984", 30, Honorific.MR);
        CityDto javaville = new CityDto("Javaville", mayor);

        StreetDto annotationsStreet = new StreetDto(javaville, "Annotations st.");
        StreetDto exceptionsAvenue = new StreetDto(javaville, "Exceptions ave.");

        CitizenDto mrOverride = new CitizenDto(annotationsStreet, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto mrsDeprecated = new CitizenDto(annotationsStreet, "Deprecated", "Williams", "875445", 30, Honorific.MRS);
        CitizenDto missSuppressWarnings = new CitizenDto(annotationsStreet, "SuppressWarnings", "Jones", "238710", 30, Honorific.MISS);
        CitizenDto mrRetention = new CitizenDto(annotationsStreet, "Retention", "Brown", "082371", 30, Honorific.MR);
        CitizenDto mrNullPointer = new CitizenDto(exceptionsAvenue, "Null Pointer", "Devis", "427341", 30, Honorific.MR);
        CitizenDto mrClassCast = new CitizenDto(exceptionsAvenue, "Class Cast", "Miller", "348540", 30, Honorific.MR);
        CitizenDto missUnsupportedOperation = new CitizenDto(exceptionsAvenue, "Unsupported Operation", "Willson", "438762", 30, Honorific.MISS);
        CitizenDto mrsNumberFormat = new CitizenDto(exceptionsAvenue, "Number Format", "Moore", "238766", 30, Honorific.MISS);

        return javaville;
    }
}
