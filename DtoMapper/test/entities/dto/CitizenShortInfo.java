package entities.dto;

import com.stu5002.dtoMapper.annotations.MappedClass;
import com.stu5002.dtoMapper.annotations.MappedField;
import entities.domain.Citizen;

/**
 * Created by stu5002 on 31.01.16.
 */
@MappedClass(Citizen.class)
public class CitizenShortInfo {

    @MappedField
    private String name;

    @MappedField("street.name")
    private String streetName;

    @MappedField("street.city.name")
    private String cityName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
