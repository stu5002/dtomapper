package entities.dto;

import com.stu5002.dtoMapper.Mapper;
import com.stu5002.dtoMapper.processors.PostProcessor;
import entities.domain.Street;

/**
 * Created by stu5002 on 08.02.16.
 */
public class StreetDtoPostProcessor implements PostProcessor {
    @Override
    public void afterToDtoMapping(Object domainObject, Object dtoObject, Mapper dtoMapper) {
        StreetDto streetDto = (StreetDto) dtoObject;
        if(streetDto.getTenants() != null){
            streetDto.setPopulation(streetDto.getTenants().size());
        }
    }

    @Override
    public void afterToDomainMapping(Object domainObject, Object dtoObject, Mapper dtoMapper) {
        Street street = (Street) domainObject;
        if(street.getTenants() != null){
            street.setPopulation(street.getTenants().size());
        }
    }
}
