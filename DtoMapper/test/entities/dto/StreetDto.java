package entities.dto;

import com.stu5002.dtoMapper.annotations.MappedClass;
import com.stu5002.dtoMapper.annotations.MappedField;
import com.stu5002.dtoMapper.annotations.PostProcessing;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stu5002 on 15.12.15.
 */
@MappedClass//(Street.class)
@PostProcessing(StreetDtoPostProcessor.class)
public class StreetDto {
    @MappedField
    private CityDto city;

    @MappedField
    private String name;

    @MappedField
    private List<CitizenDto> tenants = new ArrayList<CitizenDto>();

    private int population = 0;

    public StreetDto() {
    }

    public StreetDto(CityDto city, String name) {
        this.city = city;
        this.name = name;
        setTenants(new ArrayList<CitizenDto>());
        if(city != null){
            for(int i = 0; i < city.getStreets().length; i++){
                if(city.getStreets()[i] == null){
                    city.getStreets()[i] = this;
                    break;
                }
            }
        }
    }

    public CityDto getCity() {
        return city;
    }

    public void setCity(CityDto city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CitizenDto> getTenants() {
        return tenants;
    }

    public void setTenants(List<CitizenDto> tenants) {
        this.tenants = tenants;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}
