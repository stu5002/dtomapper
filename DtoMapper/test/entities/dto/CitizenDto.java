package entities.dto;

import com.stu5002.dtoMapper.Mapper;
import com.stu5002.dtoMapper.annotations.MappedClass;
import com.stu5002.dtoMapper.annotations.MappedField;
import com.stu5002.dtoMapper.processors.PostProcessor;
import entities.Constant;
import entities.domain.Citizen;
import entities.domain.Honorific;

import static com.stu5002.dtoMapper.Utils.isNpEquals;

/**
 * Created by stu5002 on 15.12.15.
 */
@MappedClass(Citizen.class)
public class CitizenDto implements ComparableToDomain, PostProcessor {

    @MappedField(excludeTags = {Constant.EXCLUDE_TAG})
    private String name;

    @MappedField(toDomainIgnore = true)
    private String surname;

    @MappedField(toDtoIgnore = true)
    private String phone;

    @MappedField
    private Integer age;

    @MappedField
    private Honorific honorific;

    @MappedField
    private StreetDto street;

    @MappedField("street.name")
    private String streetName;

    private String fullName;

    public CitizenDto() {
    }

    public CitizenDto(StreetDto street, String name, String surname, String phone, Integer age, Honorific honorific) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.age = age;
        this.honorific = honorific;
        this.street = street;
        if(street != null){
            street.getTenants().add(this);
        }
    }

    @Override
    public boolean isSuperficiallyEqualsTo(Object object){
        if(object instanceof Citizen){
            Citizen citizen = (Citizen) object;
            return (isNpEquals(name, citizen.getName())
                    && isNpEquals(age, citizen.getAge())
                    && isNpEquals(honorific, citizen.getHonorific()));
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Honorific getHonorific() {
        return honorific;
    }

    public void setHonorific(Honorific honorific) {
        this.honorific = honorific;
    }

    public StreetDto getStreet() {
        return street;
    }

    public void setStreet(StreetDto street) {
        this.street = street;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public void afterToDtoMapping(Object domainObject, Object dtoObject, Mapper mapper) {
        if(domainObject instanceof Citizen){
            Citizen citizen = (Citizen) domainObject;
            if(mapper.getUserData() != null && mapper.getUserData() instanceof String){
                this.setFullName((String) mapper.getUserData());
            }else {
                this.setFullName(citizen.getName() + " " + citizen.getSurname());
            }
        }
    }

    @Override
    public void afterToDomainMapping(Object domainObject, Object dtoObject, Mapper mapper) {
        Citizen citizen = (Citizen) domainObject;
        if(mapper.getUserData() != null && mapper.getUserData() instanceof String){
            citizen.setFullName((String) mapper.getUserData());
        }else {
            citizen.setFullName(this.getName() + " " + this.getSurname());
        }
    }
}
