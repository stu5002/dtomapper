package toDtoTest;

import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;
import entities.domain.City;
import entities.dto.CitizenDto;
import entities.dto.CityDto;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by stu5002 on 04.02.16.
 */
public class ProcessedObjectsCacheTest {

    private Processor processor;

    @Before
    public void setUp(){
        processor = new ToDtoProcessor(new FieldsAccessProperties());
    }

    @Test
    public void toDtoArrayCachedTest(){
        City javavilleDomain = new City("Javaville", null);
        City[] cityDomains = new City[2];
        cityDomains[0] = javavilleDomain;
        cityDomains[1] = javavilleDomain;
        CityDto[] cityDtos = processor.process(cityDomains);
        assertEquals("Test failed: ToDtoTest.ProcessedObjectsCacheTest.toDtoArrayCachedTest", cityDtos[0], cityDtos[1]);
    }

    @Test
    public void toDtoCollectionCachedTest(){
        City javavilleDomain = new City("Javaville", null);
        List<City> cityDomains = new ArrayList<City>();
        cityDomains.add(javavilleDomain);
        cityDomains.add(javavilleDomain);
        List<City> cityDtos = processor.process(cityDomains, City.class);
        assertEquals("Test failed: ToDtoTest.ProcessedObjectsCacheTest.toDtoCollectionCachedTest", cityDtos.get(0), cityDtos.get(1));
    }

    @Test
    public void toDtoCacheWithClassesTest(){
        City javavilleDomain = new City("Javaville", null);
        assertNotEquals("Test failed: ToDtoTest.ProcessedObjectsCacheTest.toDtoCacheWithClassesTest", processor.process(javavilleDomain), processor.process(javavilleDomain, CitizenDto.class));
        assertEquals("Test failed: ToDtoTest.ProcessedObjectsCacheTest.toDtoCacheWithClassesTest", (CityDto) processor.process(javavilleDomain), (CityDto) processor.process(javavilleDomain));
        assertEquals("Test failed: ToDtoTest.ProcessedObjectsCacheTest.toDtoCacheWithClassesTest", processor.process(javavilleDomain, CitizenDto.class), processor.process(javavilleDomain, CitizenDto.class));
    }
}
