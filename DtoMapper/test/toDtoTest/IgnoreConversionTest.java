package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import entities.domain.Citizen;
import entities.domain.Honorific;
import entities.dto.CitizenDto;
import org.junit.Before;
import org.junit.Test;

import static com.stu5002.dtoMapper.Utils.isNpEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class IgnoreConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void toDomainIgnoreTest(){
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.toDto(citizen);
        assertTrue("Test failed: ToDtoTest.IgnoreConversionTest.toDomainIgnoreTest", isNpEquals(citizen.getSurname(), citizenDto.getSurname()));
    }

    @Test
    public void toDtoIgnoreTest(){
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.toDto(citizen);
        assertFalse("Test failed: ToDtoTest.IgnoreConversionTest.toDtoIgnoreTest", isNpEquals(citizen.getPhone(), citizenDto.getPhone()));
    }
}
