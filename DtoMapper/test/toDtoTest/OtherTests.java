package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import entities.domain.Citizen;
import entities.domain.City;
import entities.dto.CitizenDto;
import entities.dto.CityDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class OtherTests {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void valueReplaceTest(){
        Citizen citizen = new Citizen();
        String name = "John";
        citizen.setName(name);
        CitizenDto citizenDto = new CitizenDto();
        citizenDto.setName("Adam");
        citizenDto = mapper.toDto(citizen, citizenDto);
        assertEquals("Test failed: ToDtoTest.OtherTests.valueReplaceTest", citizenDto.getName(), name);
    }
    @Test
    public void withoutConversionTest(){
        City javavilleDomain = City.buildJavaville();
        CityDto javavilleDto = mapper.toDto(javavilleDomain);
        assertTrue("Test failed: ToDtoTest.OtherTests.withoutConversionTest", javavilleDto.getMayor() == javavilleDomain.getMayor());
    }

    @Test
    public void supposititiousClassConversionTest(){
        City javavilleDomain = City.buildJavaville();
        CitizenDto citizenDto = mapper.toDto(javavilleDomain, CitizenDto.class);
        assertEquals("Test failed: ToDtoTest.OtherTests.supposititiousClassConversionTest", javavilleDomain.getName(), citizenDto.getName());
    }
}
