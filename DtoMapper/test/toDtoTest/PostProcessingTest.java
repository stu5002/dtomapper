package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import entities.domain.Citizen;
import entities.domain.City;
import entities.domain.Honorific;
import entities.domain.Street;
import entities.dto.CitizenDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created by stu5002 on 08.02.16.
 */
public class PostProcessingTest {
    DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void selfPostProcessing(){
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.toDto(citizen);
        assertEquals("Test failed: ToDtoTest.PostProcessingTest.selfPostProcessing", citizenDto.getFullName(), citizen.getName() + " " + citizen.getSurname());
    }

    @Test
    public void thirdPartyPostProcessing(){
        Street street = City.buildJavaville().getStreets()[0];
        StreetDto streetDto = mapper.toDto(street, StreetDto.class);
        assertEquals("Test failed: ToDtoTest.PostProcessingTest.thirdPartyPostProcessing", streetDto.getPopulation(), streetDto.getTenants().size());
    }

    @Test
    public void userDataTest(){
        String fakeName = "fakeName";
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.withUserData(fakeName).toDto(citizen);
        assertSame("Test failed: ToDtoTest.PostProcessingTest.userDataTest", citizenDto.getFullName(), fakeName);
    }
}
