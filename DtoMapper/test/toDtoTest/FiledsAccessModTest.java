package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.properties.FiledsAccessMod;
import entities.domain.City;
import entities.domain.CityEntityWithSubstitution;
import entities.dto.CityDto;
import entities.dto.CityDtoWithSubstitution;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by stu5002 on 04.02.16.
 */
public class FiledsAccessModTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void getValueByMethod(){
        String real = "realName";
        CityEntityWithSubstitution city = new CityEntityWithSubstitution();
        city.setRealName(real);
        mapper.getToDtoFieldsAccessProperties().setRetrievingMod(FiledsAccessMod.METHODS);
        CityDto cityDto = mapper.toDto(city);
        assertEquals("Test failed: ToDtoTest.FiledsAccessModTest.getValueByMethod", cityDto.getName(), CityEntityWithSubstitution.spoofedName);
    }

    @Test
    public void getValueDirectly(){
        String real = "realName";
        CityEntityWithSubstitution city = new CityEntityWithSubstitution();
        city.setRealName(real);
        mapper.getToDtoFieldsAccessProperties().setRetrievingMod(FiledsAccessMod.VALUES);
        CityDto cityDto = mapper.toDto(city);
        assertEquals("Test failed: ToDtoTest.FiledsAccessModTest.getValueDirectly", cityDto.getName(), real);
    }

    @Test
    public void setValueByMethod(){
        String real = "realName";
        City city = new City();
        city.setName(real);
        mapper.getToDtoFieldsAccessProperties().setInsertionMod(FiledsAccessMod.METHODS);
        CityDtoWithSubstitution cityDto = mapper.toDto(city, CityDtoWithSubstitution.class);
        assertEquals("Test failed: ToDtoTest.FiledsAccessModTest.setValueByMethod", cityDto.getRealName(), CityDtoWithSubstitution.spoofedName);
    }

    @Test
    public void setValueDirectly(){
        String real = "realName";
        City city = new City();
        city.setName(real);
        mapper.getToDtoFieldsAccessProperties().setInsertionMod(FiledsAccessMod.VALUES);
        CityDtoWithSubstitution cityDto = mapper.toDto(city, CityDtoWithSubstitution.class);
        assertEquals("Test failed: ToDtoTest.FiledsAccessModTest.setValueDirectly", cityDto.getRealName(), real);
    }
}
