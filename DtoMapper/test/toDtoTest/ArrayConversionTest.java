package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import entities.domain.City;
import entities.domain.Street;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 07.02.16.
 */
public class ArrayConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void arrayConversionTest(){
        City javaville = City.buildJavaville();
        Street[] streetDomains = javaville.getStreets();
        StreetDto[] streetDtos = mapper.toDto(streetDomains, StreetDto.class);

        for(int i = 0; i < streetDomains.length; i++){
            assertTrue("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", (streetDomains[i] == null && streetDtos[i] == null) || (streetDomains[i] != null && streetDtos[i] != null));
            if(streetDomains[i] != null){
                assertEquals("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", streetDomains[i].getName(), streetDtos[i].getName());
            }
        }
    }

    @Test
    public void nestedCollectionConversionTest(){
        City javavilleDomain = City.buildJavaville();
        CityDto javavilleDto = mapper.toDto(javavilleDomain);

        for(int i = 0; i < javavilleDomain.getStreets().length; i++){
            assertTrue("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", (javavilleDomain.getStreets()[i] == null && javavilleDto.getStreets()[i] == null) || (javavilleDomain.getStreets()[i] != null && javavilleDto.getStreets()[i] != null));
            if(javavilleDomain.getStreets()[i] != null){
                assertEquals("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", javavilleDomain.getStreets()[i].getName(), javavilleDto.getStreets()[i].getName());
            }
        }
    }
}
