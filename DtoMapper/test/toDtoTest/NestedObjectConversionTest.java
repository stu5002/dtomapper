package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import entities.domain.City;
import entities.domain.Street;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by stu5002 on 16.02.16.
 */
public class NestedObjectConversionTest {
    DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void nestedObjectTest(){
        City city = City.buildJavaville();
        Street street = city.getStreets()[0];
        StreetDto streetDto = mapper.toDto(street, StreetDto.class);
        assertEquals("Test failed: ToDtoTest.NestedObjectConversionTest.nestedObjectTest", streetDto.getCity().getClass(), CityDto.class);
    }
}
