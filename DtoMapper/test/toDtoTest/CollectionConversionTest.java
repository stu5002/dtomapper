package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import entities.domain.Citizen;
import entities.domain.City;
import entities.domain.Street;
import entities.dto.CitizenDto;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by stu5002 on 04.02.16.
 */
public class CollectionConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void collectionConversionTest(){
        List<Citizen> citizens = City.buildJavaville().getStreets()[0].getTenants();
        List<CitizenDto> citizensDtos = mapper.toDto(citizens);
        assertEquals("Test failed: ToDtoTest.CollectionConversionTest.collectionConversionTest", citizens.size(), citizensDtos.size());

        int equalsCounter = citizens.size();
        for(Citizen citizen : citizens){
            for(CitizenDto citizenDto : citizensDtos){
                if(citizenDto.isSuperficiallyEqualsTo(citizen)){
                    equalsCounter--;
                    break;
                }
            }
        }
        assertEquals("Test failed: ToDtoTest.CollectionConversionTest.collectionConversionTest", equalsCounter, 0);
    }
    @Test
    public void nestedCollectionConversionTest(){
        Street street = City.buildJavaville().getStreets()[0];
        StreetDto streetDto = mapper.toDto(street, StreetDto.class);
        assertEquals("Test failed: ToDtoTest.CollectionConversionTest.nestedCollectionConversionTest", streetDto.getTenants().size(), street.getTenants().size());

        int equalsCounter = street.getTenants().size();
        for(CitizenDto citizenDto : streetDto.getTenants()){
            for(Citizen citizen : street.getTenants()){
                if(citizenDto.isSuperficiallyEqualsTo(citizen)){
                    equalsCounter--;
                    break;
                }
            }
        }
        assertEquals("Test failed: ToDtoTest.CollectionConversionTest.nestedCollectionConversionTest", equalsCounter, 0);
    }

    @Test
    public void mergeCollectionConversionTest(){
        Street street = City.buildJavaville().getStreets()[0];
        StreetDto streetDto = new StreetDto();
        List<CitizenDto> tenants =  streetDto.getTenants();
        streetDto = mapper.toDto(street, streetDto);
        assertEquals("Test failed: ToDomainTest.CollectionConversionTest.mergeCollectionConversionTest", tenants, streetDto.getTenants());
    }

}
