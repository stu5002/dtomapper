package toDtoTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDtoProcessor;
import entities.domain.Citizen;
import entities.domain.Honorific;
import entities.dto.CitizenDto;
import org.junit.Before;
import org.junit.Test;

import static com.stu5002.dtoMapper.Utils.isNpEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class PrimitiveConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void stringMapping(){
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.toDto(citizen);
        assertTrue("Test failed: ToDtoTest.PrimitiveConversionTest.stringMapping", isNpEquals(citizen.getName(), citizenDto.getName()));
    }

    @Test
    public void autoboxingMapping(){
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.toDto(citizen);
        assertTrue("Test failed: ToDtoTest.PrimitiveConversionTest.autoboxingMapping", citizen.getAge() == citizenDto.getAge());
    }

    @Test
    public void enumMapping(){
        Citizen citizen = new Citizen(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        CitizenDto citizenDto = mapper.toDto(citizen);
        assertTrue("Test failed: ToDtoTest.PrimitiveConversionTest.enumMapping", isNpEquals(citizen.getHonorific(), citizenDto.getHonorific()));
    }
}
