package toDomainTest;

import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import com.stu5002.dtoMapper.processors.properties.FieldsAccessProperties;
import entities.domain.Citizen;
import entities.domain.City;
import entities.dto.CityDto;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by stu5002 on 04.02.16.
 */
public class ProcessedObjectsCacheTest {

    private Processor processor;

    @Before
    public void setUp(){
        processor = new ToDomainProcessor(new FieldsAccessProperties());
    }

    @Test
    public void toDomainArrayCachedTest(){
        CityDto javavilleDto = new CityDto("Javaville", null);
        CityDto[] cityDtos = new CityDto[2];
        cityDtos[0] = javavilleDto;
        cityDtos[1] = javavilleDto;
        City[] cities = processor.process(cityDtos);
        assertEquals("Test failed: ToDomainTest.ProcessedObjectsCacheTest.toDomainArrayCachedTest", cities[0], cities[1]);
    }

    @Test
    public void toDomainCollectionCachedTest(){
        CityDto javavilleDto = new CityDto("Javaville", null);
        List<CityDto> cityDtos = new ArrayList<CityDto>();
        cityDtos.add(javavilleDto);
        cityDtos.add(javavilleDto);
        List<City> cities = processor.process(cityDtos, City.class);
        assertEquals("Test failed: ToDomainTest.ProcessedObjectsCacheTest.toDomainCollectionCachedTest", cities.get(0), cities.get(1));
    }

    @Test
    public void toDomainCacheWithClassesTest(){
        CityDto javavilleDto = new CityDto("Javaville", null);
        assertNotEquals("Test failed: ToDomainTest.ProcessedObjectsCacheTest.toDomainCacheWithClassesTest", processor.process(javavilleDto), processor.process(javavilleDto, Citizen.class));
        assertEquals("Test failed: ToDomainTest.ProcessedObjectsCacheTest.toDomainCacheWithClassesTest", (City) processor.process(javavilleDto), (City) processor.process(javavilleDto));
        assertEquals("Test failed: ToDomainTest.ProcessedObjectsCacheTest.toDomainCacheWithClassesTest", processor.process(javavilleDto, Citizen.class), processor.process(javavilleDto, Citizen.class));
    }
}
