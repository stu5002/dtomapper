package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import entities.domain.City;
import entities.domain.Street;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 07.02.16.
 */
public class ArrayConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void arrayConversionTest(){
        CityDto javavilleDto = CityDto.buildJavaville();
        StreetDto[] streetDtos = javavilleDto.getStreets();
        Street[] streetDomains = mapper.toDomain(streetDtos, Street.class);

        for(int i = 0; i < streetDomains.length; i++){
            assertTrue("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", (streetDomains[i] == null && streetDtos[i] == null) || (streetDomains[i] != null && streetDtos[i] != null));
            if(streetDomains[i] != null){
                assertEquals("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", streetDomains[i].getName(), streetDtos[i].getName());
            }
        }
    }

    @Test
    public void nestedCollectionConversionTest(){
        CityDto javavilleDto = CityDto.buildJavaville();
        City javavilleDomain = mapper.toDomain(javavilleDto);

        for(int i = 0; i < javavilleDomain.getStreets().length; i++){
            assertTrue("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", (javavilleDomain.getStreets()[i] == null && javavilleDto.getStreets()[i] == null) || (javavilleDomain.getStreets()[i] != null && javavilleDto.getStreets()[i] != null));
            if(javavilleDomain.getStreets()[i] != null){
                assertEquals("Test failed: ToDomainTest.ArrayConversionTest.nestedCollectionConversionTest", javavilleDomain.getStreets()[i].getName(), javavilleDto.getStreets()[i].getName());
            }
        }
    }
}
