package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import entities.domain.Citizen;
import entities.domain.City;
import entities.dto.CitizenShortInfo;
import entities.dto.CityDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class OtherTests {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void withoutConversionTest(){
        CityDto javavilleDto = CityDto.buildJavaville();
        City javavilleDomain = mapper.toDomain(javavilleDto);
        assertTrue("Test failed: ToDomainTest.OtherTests.withoutConversionTest", javavilleDto.getMayor() == javavilleDomain.getMayor());
    }

    @Test
    public void supposititiousClassConversionTest(){
        CityDto javavilleDto = new CityDto("Javaville", null);
        Citizen citizen = mapper.toDomain(javavilleDto, Citizen.class);
        assertEquals("Test failed: ToDomainTest.OtherTests.supposititiousClassConversionTest", javavilleDto.getName(), citizen.getName());
    }

    @Test
    public void toDomainCreatePathTest(){
        CitizenShortInfo citizenInfo = new CitizenShortInfo();
        Citizen citizen = mapper.toDomain(citizenInfo);
        assertNotEquals("Test failed: ToDomainTest.OtherTests.toDomainCreatePathTest", citizen, null);

        citizenInfo = new CitizenShortInfo();
        citizenInfo.setStreetName("street");
        citizen = mapper.toDomain(citizenInfo);
        assertNotEquals("ToDomainTest.OtherTests.toDomainCreatePathTest", citizen.getStreet(), null);
        assertEquals("Test failed: ToDomainTest.OtherTests.toDomainCreatePathTest", citizen.getStreet().getCity(), null);
    }
}
