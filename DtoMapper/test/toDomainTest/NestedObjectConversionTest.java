package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import entities.domain.City;
import entities.domain.Street;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by stu5002 on 16.02.16.
 */
public class NestedObjectConversionTest {
    DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void nestedObjectTest(){
        CityDto cityDto = CityDto.buildJavaville();
        StreetDto streetDto = cityDto.getStreets()[0];
        Street street = mapper.toDomain(streetDto, Street.class);
        assertEquals("Test failed: ToDomainTest.NestedObjectConversionTest.nestedObjectTest", street.getCity().getClass(), City.class);
    }
}
