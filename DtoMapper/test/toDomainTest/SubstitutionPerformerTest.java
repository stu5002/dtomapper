package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.Mapper;
import com.stu5002.dtoMapper.Utils;
import com.stu5002.dtoMapper.substitution.SubstitutionPerformer;
import entities.domain.Citizen;
import entities.domain.City;
import entities.domain.Street;
import entities.dto.CityDto;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class SubstitutionPerformerTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    private SubstitutionPerformer getCityByNameSubstitutionPerformer(final String triggerName, final City surrogateCity){
        SubstitutionPerformer substitutionPerformer = new SubstitutionPerformer(){
            @Override
            public Object findSubstitution(Class clazz, Map<Field, Object> identifiers, Object userData) {
                if(userData != null && userData instanceof Street){
                    ((Street) userData).setCity(surrogateCity);
                }
                if(clazz == City.class){
                    for(Map.Entry<Field, Object> identifier : identifiers.entrySet()){
                        if(identifier.getKey().getName().equals("name") && Utils.isNpEquals(identifier.getValue(), triggerName)){
                            return surrogateCity;
                        }
                    }
                }
                return null;
            }
        };
        return substitutionPerformer;
    }

    @Test
    public void simpleSubstitutionTest(){
        String triggerName = "Javaville";
        City surrogateCity = new City("Oakland", null);
        SubstitutionPerformer substitutionPerformer = getCityByNameSubstitutionPerformer(triggerName, surrogateCity);
        mapper.setSubstitutionPerformer(substitutionPerformer);

        CityDto javavilleDto = new CityDto(triggerName, null);
        City javavilleDomain = mapper.toDomain(javavilleDto);
        assertTrue("Test failed: ToDomainTest.SubstitutionPerformerTest.simpleSubstitutionTest", javavilleDomain == surrogateCity);
    }

    @Test
    public void conversionByClassSubstitutionTest(){
        String triggerName = "Javaville";
        City surrogateCity = new City("Oakland", null);
        SubstitutionPerformer substitutionPerformer = getCityByNameSubstitutionPerformer(triggerName, surrogateCity);
        mapper.setSubstitutionPerformer(substitutionPerformer);

        City javavilleDomain3 = mapper.toDomain(new CityDto(triggerName, null), City.class);
        assertTrue("Test failed: ToDomainTest.SubstitutionPerformerTest.conversionByClassSubstitutionTest", javavilleDomain3 == surrogateCity);
    }

    @Test
    public void inArraySubstitutionTest(){
        String triggerName = "Javaville";
        City surrogateCity = new City("Oakland", null);
        SubstitutionPerformer substitutionPerformer = getCityByNameSubstitutionPerformer(triggerName, surrogateCity);
        mapper.setSubstitutionPerformer(substitutionPerformer);

        CityDto javavilleDto = new CityDto(triggerName, null);
        CityDto[] cityDtos = new CityDto[2];
        cityDtos[0] = javavilleDto;
        City[] citiesArray = mapper.toDomain(cityDtos);
        assertTrue("Test failed: ToDomainTest.SubstitutionPerformerTest.inArraySubstitutionTest", citiesArray[0] == surrogateCity);
    }

    @Test
    public void updateSubstitutionTest(){
        String triggerName = "Javaville";
        City surrogateCity = new City("Oakland", null);
        SubstitutionPerformer substitutionPerformer = getCityByNameSubstitutionPerformer(triggerName, surrogateCity);
        mapper.setSubstitutionPerformer(substitutionPerformer);

        City javavilleDomain2 = mapper.toDomain(new CityDto(triggerName, null), new City());
        assertTrue("Test failed: ToDomainTest.SubstitutionPerformerTest.updateSubstitutionTest", javavilleDomain2 == surrogateCity);
    }

    @Test
    public void userDataTest(){
        Street street = new Street();
        street.setCity(null);
        City surrogateCity = new City("Oakland", null);
        SubstitutionPerformer substitutionPerformer = getCityByNameSubstitutionPerformer(null, surrogateCity);
        Mapper m = mapper.withSubstitutionPerformer(substitutionPerformer)
        .withUserData(street);

        City javavilleDomain = m.toDomain(new CityDto("Javaville", null));
        assertSame("Test failed: ToDomainTest.SubstitutionPerformerTest.userDataTest", street.getCity(), surrogateCity);
    }
}
