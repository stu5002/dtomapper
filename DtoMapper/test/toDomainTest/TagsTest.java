package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import entities.Constant;
import entities.domain.Citizen;
import entities.domain.Honorific;
import entities.dto.CitizenDto;
import org.junit.Before;
import org.junit.Test;

import static com.stu5002.dtoMapper.Utils.isNpEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by stu5002 on 13.03.16.
 */
public class TagsTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void stringTagsExcludeMapping(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.withTags(Constant.EXCLUDE_TAG).toDomain(citizenDto);
        assertFalse("Test failed: ToDomainTest.PrimitiveConversionTest.stringMapping", isNpEquals(citizen.getName(), citizenDto.getName()));
    }
}
