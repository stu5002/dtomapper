package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import entities.domain.Citizen;
import entities.domain.Honorific;
import entities.dto.CitizenDto;
import org.junit.Before;
import org.junit.Test;

import static com.stu5002.dtoMapper.Utils.isNpEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class PrimitiveConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void stringMapping(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.toDomain(citizenDto);
        assertTrue("Test failed: ToDomainTest.PrimitiveConversionTest.stringMapping", isNpEquals(citizen.getName(), citizenDto.getName()));
    }

    @Test
    public void autoboxingMapping(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.toDomain(citizenDto);
        assertTrue("Test failed: ToDomainTest.PrimitiveConversionTest.autoboxingMapping", citizen.getAge() == citizenDto.getAge());
    }

    @Test
    public void enumMapping(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.toDomain(citizenDto);
        assertTrue("Test failed: ToDomainTest.PrimitiveConversionTest.enumMapping", isNpEquals(citizen.getHonorific(), citizenDto.getHonorific()));
    }

    @Test
    public void valueReplacement(){
        CitizenDto citizenDto = new CitizenDto();
        String name = "John";
        citizenDto.setName(name);
        Citizen citizen = new Citizen();
        citizen.setName("Adam");
        citizen = mapper.toDomain(citizenDto, citizen);
        assertEquals("Test failed: ToDomainTest.PrimitiveConversionTest.enumMapping", citizen.getName(), name);
    }
}
