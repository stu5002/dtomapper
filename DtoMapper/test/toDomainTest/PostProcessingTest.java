package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import entities.domain.Citizen;
import entities.domain.Honorific;
import entities.domain.Street;
import entities.dto.CitizenDto;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created by stu5002 on 08.02.16.
 */
public class PostProcessingTest {
    DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void selfPostProcessing(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.toDomain(citizenDto);
        assertEquals("Test failed: ToDomainTest.PostProcessingTest.selfPostProcessing", citizen.getFullName(), citizenDto.getName() + " " + citizenDto.getSurname());
    }

    @Test
    public void thirdPartyPostProcessing(){
        StreetDto streetDto = CityDto.buildJavaville().getStreets()[0];
        Street street = mapper.toDomain(streetDto, Street.class);
        assertEquals("Test failed: ToDomainTest.PostProcessingTest.thirdPartyPostProcessing", street.getPopulation(), street.getTenants().size());
    }

    @Test
    public void userDataTest(){
        String fakeName = "fakeName";
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.withUserData(fakeName).toDomain(citizenDto);
        assertSame("Test failed: ToDomainTest.PostProcessingTest.userDataTest", citizen.getFullName(), fakeName);
    }
}
