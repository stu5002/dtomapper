package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.properties.FiledsAccessMod;
import entities.domain.City;
import entities.domain.CityEntityWithSubstitution;
import entities.dto.CityDto;
import entities.dto.CityDtoWithSubstitution;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by stu5002 on 04.02.16.
 */
public class FiledsAccessModTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void getValueByMethod(){
        String real = "realName";
        CityDtoWithSubstitution cityDto = new CityDtoWithSubstitution();
        cityDto.setRealName(real);
        mapper.getToDomainFieldsAccessProperties().setRetrievingMod(FiledsAccessMod.METHODS);
        City city = mapper.toDomain(cityDto);
        assertEquals("Test failed: ToDomainTest.FiledsAccessModTest.getValueByMethod", city.getName(), CityDtoWithSubstitution.spoofedName);
    }

    @Test
    public void getValueDirectly(){
        String real = "realName";
        CityDtoWithSubstitution cityDto = new CityDtoWithSubstitution();
        cityDto.setRealName(real);
        mapper.getToDomainFieldsAccessProperties().setRetrievingMod(FiledsAccessMod.VALUES);
        City city = mapper.toDomain(cityDto);
        assertEquals("Test failed: ToDomainTest.FiledsAccessModTest.getValueDirectly", city.getName(), real);
    }

    @Test
    public void setValueByMethod(){
        String real = "realName";
        CityDto cityDto = new CityDto();
        cityDto.setName(real);
        mapper.getToDomainFieldsAccessProperties().setInsertionMod(FiledsAccessMod.METHODS);
        CityEntityWithSubstitution city = mapper.toDomain(cityDto, CityEntityWithSubstitution.class);
        assertEquals("Test failed: ToDomainTest.FiledsAccessModTest.setValueByMethod", city.getRealName(), CityEntityWithSubstitution.spoofedName);
    }

    @Test
    public void setValueDirectly(){
        String real = "realName";
        CityDto cityDto = new CityDto();
        cityDto.setName(real);
        mapper.getToDomainFieldsAccessProperties().setInsertionMod(FiledsAccessMod.VALUES);
        CityEntityWithSubstitution city = mapper.toDomain(cityDto, CityEntityWithSubstitution.class);
        assertEquals("Test failed: ToDomainTest.FiledsAccessModTest.setValueDirectly", city.getRealName(), real);
    }
}
