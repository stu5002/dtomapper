package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import entities.domain.Citizen;
import entities.domain.Honorific;
import entities.dto.CitizenDto;
import org.junit.Before;
import org.junit.Test;

import static com.stu5002.dtoMapper.Utils.isNpEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by stu5002 on 04.02.16.
 */
public class IgnoreConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void toDomainIgnore(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.toDomain(citizenDto);
        assertFalse("Test failed: ToDomainTest.IgnoreConversionTest.toDomainIgnore", isNpEquals(citizen.getSurname(), citizenDto.getSurname()));
    }

    @Test
    public void toDtoIgnore(){
        CitizenDto citizenDto = new CitizenDto(null, "Override", "Johnson", "242349", 30, Honorific.MR);
        Citizen citizen = mapper.toDomain(citizenDto);
        assertTrue("Test failed: ToDomainTest.IgnoreConversionTest.toDtoIgnore", isNpEquals(citizen.getPhone(), citizenDto.getPhone()));
    }
}
