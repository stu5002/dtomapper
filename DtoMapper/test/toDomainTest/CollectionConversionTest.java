package toDomainTest;

import com.stu5002.dtoMapper.DtoMapper;
import com.stu5002.dtoMapper.processors.Processor;
import com.stu5002.dtoMapper.processors.ToDomainProcessor;
import entities.domain.Citizen;
import entities.domain.Street;
import entities.dto.CitizenDto;
import entities.dto.CityDto;
import entities.dto.StreetDto;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by stu5002 on 04.02.16.
 */
public class CollectionConversionTest {

    private DtoMapper mapper;

    @Before
    public void setUp(){
        mapper = new DtoMapper();
    }

    @Test
    public void collectionConversionTest(){
        CityDto javavilleDto = CityDto.buildJavaville();
        List<CitizenDto> citizensDtos = javavilleDto.getStreets()[0].getTenants();
        List<Citizen> citizens = mapper.toDomain(citizensDtos);
        assertEquals("Test failed: ToDomainTest.CollectionConversionTest.collectionConversionTest", citizens.size(), citizensDtos.size());

        int equalsCounter = citizens.size();
        for(Citizen citizen : citizens){
            for(CitizenDto citizenDto : citizensDtos){
                if(citizenDto.isSuperficiallyEqualsTo(citizen)){
                    equalsCounter--;
                    break;
                }
            }
        }
        assertEquals("Test failed: ToDomainTest.CollectionConversionTest.collectionConversionTest", equalsCounter, 0);
    }

    @Test
    public void nestedCollectionConversionTest(){
        CityDto javavilleDto = CityDto.buildJavaville();
        StreetDto streetDto = javavilleDto.getStreets()[0];
        Street street = mapper.toDomain(streetDto, Street.class);
        assertEquals("Test failed: ToDomainTest.CollectionConversionTest.nestedCollectionConversionTest", streetDto.getTenants().size(), street.getTenants().size());

        int equalsCounter = street.getTenants().size();
        for(Citizen citizen : street.getTenants()){
            for(CitizenDto citizenDto : streetDto.getTenants()){
                if(citizenDto.isSuperficiallyEqualsTo(citizen)){
                    equalsCounter--;
                    break;
                }
            }
        }
        assertEquals("Test failed: ToDomainTest.CollectionConversionTest.nestedCollectionConversionTest", equalsCounter, 0);
    }

    @Test
    public void mergeCollectionConversionTest(){
        CityDto javavilleDto = CityDto.buildJavaville();
        StreetDto streetDto = javavilleDto.getStreets()[0];
        Street street = new Street();
        List<Citizen> tenants =  street.getTenants();
        street = mapper.toDomain(streetDto, street);
        assertEquals("Test failed: ToDomainTest.CollectionConversionTest.mergeCollectionConversionTest", tenants, street.getTenants());
    }
}
